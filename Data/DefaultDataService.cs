using Avalonia.Controls;
using Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using Microsoft.Win32;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Text;

namespace Data
{
    public class DefaultDataService : IDataService
    {
        public PersonOfInterest GetSurveyFromFile(string surveyFile)
        {
            using(FileStream sourceStream = new FileStream(surveyFile, FileMode.Open, FileAccess.Read, FileShare.Read, bufferSize: 4096, useAsync: true))
            {                
                PersonOfInterest poi = new PersonOfInterest();

                byte[] buffer = new byte[0x1000];
                int numRead;
                
                while((numRead = sourceStream.Read(buffer, 0, buffer.Length)) != 0)
                {
                    string text = Encoding.Unicode.GetString(buffer, 0, numRead);
                    return new PersonOfInterest(text, text, text, text, text);
                
                }
                return new PersonOfInterest("error","error","error","error","error");
            }
        }

        public bool FileExists(string surveyFile)
        {
            return File.Exists(surveyFile);
        }

        public async Task<string> FindFileAsync()
        {
            var openFileDialog = new OpenFileDialog()
            {
                AllowMultiple = false,
                Title="What img file do you want to use"
            };
            
            var pathArray = await openFileDialog.ShowAsync();

            if ((pathArray?.Length ?? 0) > 0)
            {
                return pathArray[0];
            }
            return null;
        }
    }
}