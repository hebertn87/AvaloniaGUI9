using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces
{
    public class PersonOfInterest
    {
        public PersonOfInterest()
        {
            Fname = "";
            Lname = "";
            GameOfChoice = "";
            SkillLvl = "";
            InterestLvl = "";
        }
        
        public PersonOfInterest(string fname, string lname, string gameOfChoice, string skillLvl, string interestLvl)
        {
            Fname = fname;
            Lname = lname;
            GameOfChoice = gameOfChoice;
            SkillLvl = skillLvl;
            InterestLvl = interestLvl;
        }

        public string Fname { get; }
        public string Lname{ get; }
        public string GameOfChoice{ get; }
        public string SkillLvl { get ;}
        public string InterestLvl { get; }
    }
}