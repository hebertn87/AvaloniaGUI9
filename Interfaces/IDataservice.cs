using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Interfaces
{
    public interface IDataService
    {
        PersonOfInterest GetSurveyFromFile(string surveyFile);
        bool FileExists(string surveyFile);
        Task<string> FindFileAsync();
    }
}