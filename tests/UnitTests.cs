using NUnit.Framework;
using System;
using app;
using Interfaces;
using Moq;
using FluentAssertions;

namespace Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {

        }

        public static string TestString = null;

        [Test]
        public void TestSimpleCommand(){
            var command = new SimpleCommand(()=> Tests.TestString="Create command worked!");
            command.Execute(this);
            Assert.AreEqual("Create command worked!", TestString);
        }   

        [Test]
        public void TestReadFromFile(){
            var dataMock = new Mock<IDataService>();
            dataMock.Setup(m => m.GetSurveyFromFileAsync(It.IsAny<string>()))
                .ReturnsAsync(new []
                {
                    new PersonOfInterest("Nathan", "Hebert", "Overwatch", "10", "7")
                });

            dataMock.Setup(m => m.FileExists(It.IsAny<String>())).Returns(true);

            var vm = new MainViewModel(dataMock);

            var.FileNamePath = "Whatever";

            Assert.IsTrue(vm.LoadFile.CanExecute(this));

            vm.LoadFile.Execute(this);
            Assert.AreEqual(vm.Output, $"We found {vm.People.Count} people in file {vm.FileNamePath}");
        }

        [Test]
        public CanLoadFile()
        {
            var dataMock = new Mock<IDataService>();

            dataMock.Setup(m => m.FindfileAsync()).ReturnsAsync("/swag/swag");
            dataMock.Setup(m => m.FileExists(It.IsAny<String>())).Returns(true);

            var vm = new MainViewModel(dataMock.Object);

            Assert.IsTrue(vm.LoadFile.CanExecute(this));
        }
    }
}