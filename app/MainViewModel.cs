using Interfaces;
using Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.ComponentModel;

namespace GUI9
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public MainViewModel() : this(new DefaultDataService())
        {}

        public MainViewModel(IDataService data)
        {
            this.data = data ?? throw new ArgumentNullException(nameof(data));
            Person = new ObservableCollection<PersonOfInterest>();
        }

        public ObservableCollection<PersonOfInterest> Person{ get; set;}

        private void OnPropertyChanged(string propertyName)=>PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        public event PropertyChangedEventHandler PropertyChanged;

        private string fileNamePath;
        public string FileNamePath
        {
            get => fileNamePath;
            set{
                fileNamePath = value;
                OnPropertyChanged(nameof(FileNamePath));
                LoadFile.RaiseCanExecuteChanged();
            }
        }

        private string output;
        public string Output{
            get => output;
            set{
                output = value;
                OnPropertyChanged(nameof(Output));
            }
        }
        
        private readonly IDataService data;

        private SimpleCommand loadFile;
        public SimpleCommand LoadFile => loadFile ?? (loadFile =  new SimpleCommand(
            () => !IsBusy && data.FileExists(FileNamePath),
            async () => 
            {
                Output = "Loading...";
                IsBusy = true;
                Person.Add(data.GetSurveyFromFile(FileNamePath));

                Output = $"We found {Person.Count} people in the file {FileNamePath}";
                IsBusy = false;
                    
            }
        ));

        private SimpleCommand findFile;
        private SimpleCommand FindFile => findFile ?? (findFile = new SimpleCommand(
                () => !IsBusy,
                async () =>
                {
                    fileNamePath = await data.FindFileAsync();
                    LoadFile.RaiseCanExecuteChanged();
                }
        ));
        
        private bool isBusy;
        public bool IsBusy{
            get => isBusy;
            set{
                isBusy = value;
                OnPropertyChanged(nameof(IsBusy));
                LoadFile.RaiseCanExecuteChanged();
                FindFile.RaiseCanExecuteChanged();
            }
        }

        private PersonOfInterest selectedPerson;
        public PersonOfInterest SelectedPerson
        {
            get => selectedPerson;
            set
            {
                selectedPerson = value;
                OnPropertyChanged(nameof(SelectedPerson));
            }
        }
    }
}

